// Shane Acoveno
// 2/2/18
// CSE2-210

// Welcome Class
public class WelcomeClass {
    	// main method
   	public static void main(String[] args) {
// prints "WELCOME" with frame around it
			
			
			boolean test = 2 < 3 || 5*2==11 && 5 > 6;
			System.out.println(test);
      System.out.println("   -----------");
      System.out.println("   | WELCOME | ");
      System.out.println("   -----------");
// prints top half of frame above lehigh network id
      System.out.println("   ^  ^  ^  ^  ^  ^");
      System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\ ");
// prints lehigh network id
      System.out.println(" <-S--M--A--2--2--1->");
// prints bottom half of frame below lehigh network id
      System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ / ");
      System.out.println("   v  v  v  v  v  v");
// prints autobiographic statement
     System.out.println("My name is Shane Acoveno and I'm from Milford, Pennsylvania. I'm a freshman looking to pursue a degree in either computer science or computer engineering. Also, I'm a member of the basketball team.");
	}  //end of main method   
} //end of class
