=============================
grade7.txt
=============================
Grade: 95/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
The code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The code runs somewhat properly. Got stuck in a never ending loop when entering wrong input. 

C) How can any runtime errors be resolved?
N/A

D) What topics should the student study in order to avoid the errors they made in this homework?
Loops

E) Other comments:
