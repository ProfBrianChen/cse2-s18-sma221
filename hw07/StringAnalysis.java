// Shane Acoveno
//3/26/18
//CSE 2 String Analysis

//import scanner class
import java.util.Scanner;

//hw07: String Analysis
public class StringAnalysis {
  
  //method for just the string type
  public static boolean Analyze(String input){
    int length = input.length(); //get the length of the string
    Character currentChar;
    boolean allLetters = true;
    //run for loop until end of string
    for(int i = 0; i < length; i++){
    
    currentChar = input.charAt(i);
    if(Character.isLetter(currentChar)){
      }
    else{
      i = length;
      allLetters = false;
    }
    }
    return allLetters;
  }
  
  //method for the string and integer types
  public static boolean Analyze(String input, int number){
    int length = input.length(); //get the length of the string
    
    if (number < length){ //if integer inputted is less than length use integer in for loop
      length = number;
    }
    
    Character currentChar;
    boolean allLetters = true;
    //run for loop until end of string
    for(int i = 0; i < length; i++){
    
    currentChar = input.charAt(i);
    if(Character.isLetter(currentChar)){
      }
    else{
      i = length;
      allLetters = false;
    }
    }
    return allLetters;
  }
 
  
  
  //main method
  public static void main(String[] args){
    
    //create instance that will take input from STDIN
    Scanner myScanner = new Scanner( System.in );
    
    //prompt user for input as a string
    System.out.println("Enter a string to determine if its characters are letters: ");
    String input = myScanner.next();
    
    //prompt user for if they want the entire string analyzed or an integer number of characters 
    System.out.println("If you want to analyze all the characters of the string enter a non positive integer: ");
    System.out.println("If you want to analyze a certain number of characters enter the amount as a positive integer: ");
    
    int number = 1;
    boolean verify = false;
    
    while(!verify){
    if(myScanner.hasNextInt()){
      number = myScanner.nextInt();
      if(number > 0){
        verify = true;
       System.out.println(Analyze(input,number)); //call analyze method for string and int types and print return
        
      }
      else{
        verify = true;
        System.out.println(Analyze(input)); //call analyze method for just string type and print return
      }
      
    }
    else{
    System.out.println("Error: If you want to analyze all the characters of the string enter a non positive integer: ");
    System.out.println("Error: If you want to analyze a certain number of characters enter the amount as a positive integer: ");
    }
    
    }
  }
 }