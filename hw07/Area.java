// Shane Acoveno
//3/26/18
//CSE 2 Area

//import scanner class
import java.util.Scanner;

//hw07: area
public class Area {
  
 //method for verifying double input
 public static double DoubleInput(){
 
//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );
   
 boolean isAdouble = false;
 double dub = 1;
 String junkWord = "";
 //run while loop until positive double is entered
while (!isAdouble){
  if (myScanner.hasNextDouble()){
  dub = myScanner.nextDouble();
  if(dub > 0){
    isAdouble = true;
    }
  else {
    System.out.println("ERROR: Please enter the input as a positive double");
}
  }
else {
  junkWord = myScanner.next();
  System.out.println("ERROR: Please enter the input as a positive double");
}
  }
   return dub;
 }
  
  //method for calculating area of a rectangle
  public static void RectangleArea(){
  double width = 1;
  double height = 1;
  //prompt user for width and height as doubles
  System.out.println("Enter the width of the rectangle as a double: ");
  width = DoubleInput(); //call double input method
 System.out.println("Enter the height of the rectangle as a double: ");
  height = DoubleInput(); //call double input method
  
  
  //calculate and display area
  double rectArea = width * height;
  System.out.println("The area of the rectangle is: "+rectArea);
  
  }
  
  //method for calculating area of a triangle
  public static void TriangleArea(){
    double baseLength = 1;
    double height = 1;
    //prompt for user to enter inputs as doubles
    System.out.println("Enter the length of the base as a double: ");
    baseLength = DoubleInput();
    System.out.println("Enter the height of the triangle as a double:");
    height = DoubleInput();
    
    //calculate and display area
    double triArea = (baseLength/2) * height;
    System.out.println("The area of the triangle is: " + triArea);
  }
  
  
  //method for calculating area of a circle
  public static void CircleArea(){
    double radius = 1;
    //prompt user to enter the radius as a double
    System.out.println("Enter the radius of the circle as a double: ");
    radius = DoubleInput();
    
    //calculate and display area
    double cArea = Math.PI * (radius * radius);
    System.out.println("The area of the circle is: " + cArea);   
  }
  
  //main method
  public static void main(String[] args){
    
  //create instance that will take input from STDIN
  Scanner myScanner = new Scanner( System.in );
  
  //input verification variables
  boolean rectangle = false;
  boolean triangle = false;
  boolean circle = false;
  String r = "rectangle";
  String t = "triangle";
  String c = "circle";
    
  String input = "";
  
  //initial input verification
  //run while loop until proper strings are entered
  System.out.println("Please enter the name of the shape whose area you would like calculated in lowercase:");
  while(!rectangle && !triangle && !circle){
    if(myScanner.hasNext()){
      input = myScanner.next();
      if(input.equals(r)){ //check to see if input equals rectangle
        rectangle = true;
        RectangleArea(); //since rectangle was inputted, call its area method
        }
      else if(input.equals(t)){ //check to see if input equals triangle
        triangle = true;
        TriangleArea(); //since triangle was inputted, call its area method
        }
      else if(input.equals(c)){ //check to see if input equals circle
        circle = true;
        CircleArea(); //since circle was inputted, call its area method
        }
      else{ //if inputs are none of the above, prompt user for new input
        input = "";
        System.out.println("Incorrect input entered. Available shapes are rectangle, triangle, and circle. Please enter all lowercase:");
      }
      }
    }
  }
  }
 
  
  
