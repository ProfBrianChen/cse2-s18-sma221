// Shane Acoveno
// 2/16/18
// CSE2-210
// hw04

//Yahtzee
//Purpose:
//

//import Scanner class
import java.util.Scanner;

public class Yahtzee{
    	// main method required for every Java program
   	  public static void main(String[] args) {
        //
        int numOne = 0;
        int numTwo = 0;
        int numThree = 0;
        int numFour = 0;
        int numFive = 0;
        
        
        //create instance that will take input from STDIN
        Scanner myScanner = new Scanner( System.in );
        
        //Prompt the user for random roll or user inputted roll
        System.out.print("Enter '1' for a user inputed roll. Enter '2' for a random roll. ");
          
        //accept user input as an Int
        int userChoice = myScanner.nextInt();
        
        //use if else statements to either generate random roll or accept user roll
        if (userChoice == 1){
          System.out.println("Enter a 5 digit number representing the result of a specific roll (e.g 13546) : ");
          
          //accept user input as a string
          String roll = myScanner.next();
          
          //check length of input and verify each number
          if (roll.length() == 5){
            //return each character from string
            char sOne = roll.charAt(0);
            char sTwo = roll.charAt(1);
            char sThree = roll.charAt(2);
            char sFour = roll.charAt(3);
            char sFive = roll.charAt(4);
            
            //convert each character to an integer
            numOne = Character.getNumericValue(sOne);
             numTwo = Character.getNumericValue(sTwo);
             numThree = Character.getNumericValue(sThree);
             numFour = Character.getNumericValue(sFour);
             numFive = Character.getNumericValue(sFive);
            
            //verify that each number is between 1 and 6 and if not, exit the program
            if (numOne  < 1 || numOne > 6){
              System.out.println("Incorrect number entered.");
              System.exit(0);
            }
            else if(numTwo  < 1 || numTwo > 6){
              System.out.println("Incorrect number entered.");
              System.exit(0);
            }
            else if(numThree  < 1 || numThree > 6){
              System.out.println("Incorrect number entered.");
              System.exit(0);
            }
            else if(numFour  < 1 || numFour > 6){
              System.out.println("Incorrect number entered.");
              System.exit(0);
            }
            else if(numFive  < 1 || numFive > 6){
              System.out.println("Incorrect number entered.");
              System.exit(0);
            }
         
         //if the number of digits is not equal to 5, exit the program
          }
          else{
            System.out.println("Incorrect number of digits inputted.");
            System.exit(0);
          }
          
        }
        
        else if (userChoice == 2){
          //generate 5 random numbers from 1-6
           numOne = (int) (Math.random() * 6)+1;
           numTwo = (int) (Math.random() * 6)+1;
          numThree = (int) (Math.random() * 6)+1;
          numFour = (int) (Math.random() * 6)+1;
           numFive = (int) (Math.random() * 6)+1;
          System.out.println("Your random roll is: " + numOne + numTwo + numThree + numFour + numFive);
        }
        //if user does not choose an inputed roll or a random roll, exit the program
        else {
          System.out.println("Incorrect input entered.");
          System.exit(0);
        }
        
     //count the number of occurences of each number 
     //initialize counters
     int oneCount = 0;
     int twoCount = 0;
     int threeCount = 0;
     int fourCount = 0;
     int fiveCount = 0;
     int sixCount = 0;
      
     //use switch statement for each individual integer in order to count occurences
     switch(numOne){
       case 1:
         ++oneCount;
       break;
       case 2:
         ++twoCount;
       break; 
       case 3:
         ++threeCount;
       break;
       case 4:
         ++fourCount;
       break;
       case 5:
         ++fiveCount;
       break;
       case 6:
         ++sixCount;
       break;
     }
        switch(numTwo){
       case 1:
         ++oneCount;
       break;
       case 2:
         ++twoCount;
       break; 
       case 3:
         ++threeCount;
       break;
       case 4:
         ++fourCount;
       break;
       case 5:
         ++fiveCount;
       break;
       case 6:
         ++sixCount;
       break;
     }
        switch(numThree){
       case 1:
         ++oneCount;
       break;
       case 2:
         ++twoCount;
       break; 
       case 3:
         ++threeCount;
       break;
       case 4:
         ++fourCount;
       break;
       case 5:
         ++fiveCount;
       break;
       case 6:
         ++sixCount;
       break;
     }
        switch(numFour){
       case 1:
         ++oneCount;
       break;
       case 2:
         ++twoCount;
       break; 
       case 3:
         ++threeCount;
       break;
       case 4:
         ++fourCount;
       break;
       case 5:
         ++fiveCount;
       break;
       case 6:
         ++sixCount;
       break;
     }
        switch(numFive){
       case 1:
         ++oneCount;
       break;
       case 2:
         ++twoCount;
       break; 
       case 3:
         ++threeCount;
       break;
       case 4:
         ++fourCount;
       break;
       case 5:
         ++fiveCount;
       break;
       case 6:
         ++sixCount;
       break;
     }
     
     //print the number counts to ensure calculations are correct
     System.out.println("One's count: "+oneCount);
     System.out.println("Two's count: "+twoCount);
     System.out.println("Three's count: "+threeCount);
     System.out.println("Four's count: "+fourCount);
     System.out.println("Five's count: "+fiveCount);
     System.out.println("Six's count: "+sixCount);
     
     //upper section initial total
        int scoreOnes = oneCount * 1;
        int scoreTwos = twoCount * 2;
        int scoreThrees = threeCount * 3;
        int scoreFours = fourCount * 4;
        int scoreFives = fiveCount * 5;
        int scoreSixes = sixCount * 6;
        int upperInitial = scoreOnes+scoreTwos+scoreThrees+scoreFours+scoreFives+scoreSixes;
        
        //display the upper initial score 
        System.out.println("Upper Section Initial Score: " + upperInitial);
        
        //determine if bonus has been reached and calculate and display upper total score
        int upperTotal = 0;
        if (upperInitial >= 63){
          int bonus = 35;
          upperTotal = upperInitial + bonus;
          System.out.println("Bonus: "+ bonus);
          System.out.println("Upper Section Total Score: " + upperTotal);
        }
        else{
          upperTotal = upperInitial;
          System.out.println("Bonus: 0");
          System.out.println("Upper Section Total Score: " + upperTotal);
        }
        
        //calculate 3 of a kind and display
        int threeKind = 0;
        
        if (oneCount == 3){
          threeKind = scoreOnes;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else if (twoCount == 3){
          threeKind = scoreTwos;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else if (threeCount == 3){
          threeKind = scoreThrees;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else if (fourCount == 3){
          threeKind = scoreFours;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else if (fiveCount == 3){
          threeKind = scoreFives;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else if (sixCount == 3){
          threeKind = scoreSixes;
          System.out.println("Three of a kind score: "+ threeKind);
        }
        else {
          threeKind = 0;
          System.out.println("Three of a kind score: " + threeKind);
        }
        
        //calculate four of a kind and display
        int fourKind = 0;
        if (oneCount == 4){
          fourKind = scoreOnes;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else if (twoCount == 4){
          fourKind = scoreTwos;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else if (threeCount == 4){
          fourKind = scoreThrees;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else if (fourCount == 4){
          fourKind = scoreFours;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else if (fiveCount == 4){
          fourKind = scoreFives;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else if (sixCount == 4){
          fourKind = scoreSixes;
          System.out.println("Four of a kind score: "+ fourKind);
        }
        else {
          
          System.out.println("Four of a kind score: " + fourKind);
        }
        
        //create full house boolean
        boolean fullHouse = (threeKind > 0) && (oneCount == 2 || twoCount == 2 || threeCount == 2 || fourCount == 2 || fiveCount == 2 || sixCount ==2);
        
        //determine and display full house score
        int fullScore = 0;
        if(fullHouse){
          fullScore = 25;
          System.out.println("Full house score: "+ fullScore);
        }
        else{
          System.out.println("Full house score: "+ fullScore);
        }
         
        //determine small or large straight and display scores
        boolean largeStraight = oneCount  <= 1 && twoCount == 1 && threeCount == 1 && fourCount == 1 && fiveCount == 1 && sixCount <= 1;
        boolean smallStraight = (oneCount  >= 1 && twoCount >= 1 && threeCount >=1 && fourCount >=1) || (fiveCount  >= 1 && twoCount >= 1 && threeCount >=1 && fourCount >=1) || (fiveCount  >= 1 &&sixCount >= 1&& threeCount >=1&& fourCount >=1);
        
        int largeScore = 0;
        int smallScore = 0;
        if(largeStraight){
          largeScore = 40;
          System.out.println("Small straight score: "+ smallScore);
          System.out.println("Large straight score: " +largeScore);
          
        }
        else if(smallStraight){
          smallScore = 30;
          System.out.println("Small straight score: "+ smallScore);
          System.out.println("Large straight score: " +largeScore);
        }
        else{
          System.out.println("Small straight score: "+ smallScore);
          System.out.println("Large straight score: " +largeScore);
        }
        
        //determine yahtzee and display the score
        boolean yahtzee = oneCount == 5 || twoCount == 5 || threeCount == 5 || fourCount ==5 || fiveCount ==5 || sixCount ==5;
        
        int yScore = 0;
        if(yahtzee){
          yScore = 50;
          System.out.println("Yahtzee score: " + yScore);
        }
        else{
          System.out.println("Yahtzee score: " + yScore);
        }
      
        //determine and display chance which is equal to upper initial score 
        int chance = upperInitial;
        System.out.println("Chance score: "+ chance);
        
        //calculate and display lower section score
        int lowerScore = threeKind + fourKind + fullScore + smallScore + largeScore + yScore + chance;
        System.out.println("Lower Section Score: " + lowerScore);
        
        //calculate and determine grand total
        int grandTotal = upperTotal + lowerScore;
        System.out.println("Grand Total: " + grandTotal);
        
        

}  //end of main method   
  	} //end of class