// Shane Acoveno
// 3/2/18
// CSE2-210

//import Scanner class
import java.util.Scanner;

// Twist Generator
public class TwistGenerator {
    	// main method
   	public static void main(String[] args) {
     
//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );
      
//check if input is an integer
boolean correct = false;
int length = 0;

//run while loop until proper input is recieved
while (!correct){
  System.out.println("Enter a positive integer: ");
  if (myScanner.hasNextInt()){
  length = myScanner.nextInt();
  if(length > 0){
    correct = true;
  }
  else{
    length = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}
     
int counter = 0;
//get the remainder of 3 from the length
int modThree = length % 3;
// get the number of threes
int threes = length / 3;

//print the first line
while (counter < threes){
counter += 1;
System.out.print("\\ /");
}

switch(modThree){
  case 1:
      System.out.print("\\");
      break;
  case 2:
      System.out.print("\\ ");
      break;
}

//print the second line
System.out.println("");
counter = 0;
while (counter < threes){
counter += 1;
System.out.print(" X ");
}

//print the third line
System.out.println("");
counter = 0;      
while (counter < threes){
counter += 1;
System.out.print("/ \\");
}
      
switch(modThree){
  case 1:
      System.out.print("/");
      break;
  case 2:
      System.out.print("/ ");
      break;
}
System.out.println("");
    }
} //end of class
