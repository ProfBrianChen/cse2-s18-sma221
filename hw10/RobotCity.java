//Shane Acoveno
//HW 10 - Robot City
// 4/23/18

public class RobotCity {
  
  //build city method
  public static int[][] buildCity(){
    
    //randomly generate east-west & north-south dimensions between 10 & 15
    final int east_west = (int) (Math.random() * ((15 - 10)+1))+ 10;
    final int north_south = (int) (Math.random() * ((15 - 10)+1))+ 10;
    System.out.println(" DIMENSIONS ");
    System.out.println("East-West: "+east_west);
    System.out.println("North-South: "+north_south);
    //declarate and allocate the two dimension rectangular array
    int [][] cityBlock = new int [east_west] [north_south];
    
    //assign the population to each block
    for(int i = 0; i < east_west; i++){
      for(int j = 0; j < north_south; j++){
        //randomly generate population to assigned to each city block
        int population = (int) (Math.random() * ((999 - 100)+1))+ 100;
        cityBlock [i][j] = population;
      }
    }
    return cityBlock;}
  
  //display method - prints out population of each block
  public static void display(int[][] cityBlock){
    
  //get lengths of array to use in for loop
  int firstDim = cityBlock.length;
  int secondDim = cityBlock[0].length; //since rectangular array
  System.out.println("");  
  //print block coordinates & population
  for(int i = 0; i < firstDim; i++){
    for(int j = secondDim - 1; j > -1; j--){
      int population = cityBlock[i][j];
      System.out.printf(" %4d", population);
    }
    System.out.println("");
  }
  }
  
  //invade method
  public static int [][] invade(int[][] cityBlock, int k){
    while(k > 0){
      //generate random coordinates for the kth robot
      int ewMAX = cityBlock.length - 1;
      int nsMAX = cityBlock[0].length - 1; //since rectangular array
      int east_west_coordinate = (int) (Math.random() * ewMAX );
      int north_south_coordinate = (int) (Math.random() * nsMAX);
      int ogValue = cityBlock [east_west_coordinate] [north_south_coordinate];
      //check to see if another robot has landed at such coordinate
      if(ogValue > 0){
        int robotValue = ogValue * -1;
        cityBlock [east_west_coordinate] [north_south_coordinate] = robotValue;
        k--;
      }
      
    }
    return cityBlock;}
  
  //update method
  public static int [][] update(int[][] cityBlock){
    
  //get lengths of array to use in for loop
  int firstDim = cityBlock.length;
  int secondDim = cityBlock[0].length; //since rectangular array  
    
  for(int i = 0; i < firstDim; i++){
    for(int j = secondDim - 1; j > -1; j--){
      int population = cityBlock[i][j];
      //check to see if population has a robot (negative value)
      if(population < 0){
        cityBlock[i][j] = population * -1; //make robots go away by turning positive
        int populationTwo = cityBlock[i][j+1];
        cityBlock[i][j+1] = populationTwo * -1;
      }
      }
     }
    return cityBlock;}
  
  //main method
  public static void main(String[] args){
    
    //run 5 times with a loop
    for(int r = 0; r<5; r++){
    //call buildCity
    int [][] cityBlock = buildCity();
    
    //call display
    display(cityBlock);
    
    //generate between 40 and 80 robots for k value
    int k = (int) (Math.random() * ((80 - 40)+1))+ 40;
    System.out.println("");
    System.out.println("Number of robots: " + k);
    
    //call invade
    cityBlock = invade(cityBlock,k);
    
    //call display
    display(cityBlock);
    
    //call update
    cityBlock = update(cityBlock);
    System.out.println("");
    System.out.println("Update: ");
    
    //call display
    display(cityBlock);
    }  
  }
}