// Shane Acoveno
// 3/17/18
// CSE2-210
//hw06

//import Scanner class
import java.util.Scanner;

public class Argyle {
    	// main method 
   	public static void main(String[] args) {
      
//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );


//obtain viewing width value as positive integer
boolean correct = false;
int widthWindow = 0;
//run while loop until proper input is recieved
while (!correct){
  System.out.println("Please enter a positive integer for the width of the viewing window in characters: ");
  if (myScanner.hasNextInt()){
  widthWindow = myScanner.nextInt();
  if(widthWindow > 0){
    correct = true;
  }
  else{
    widthWindow = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}
      
//obtain viewing height value as positive integer
correct = false;
int heightWindow = 0;
//run while loop until proper input is recieved
while (!correct){
  System.out.println("Please enter a positive integer for the height of the viewing window in characters: ");
  if (myScanner.hasNextInt()){
  heightWindow = myScanner.nextInt();
  if(heightWindow > 0){
    correct = true;
  }
  else{
    heightWindow = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}

//obtain diamond width value as a positive integer
correct = false;
int widthDiamond = 0;
//run while loop until proper input is recieved
while (!correct){
  System.out.println("Please enter a positive integer for the width of the argyle diamonds: ");
  if (myScanner.hasNextInt()){
  widthDiamond = myScanner.nextInt();
  if(widthDiamond > 0){
    correct = true;
  }
  else{
    widthDiamond = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}
      
//obtain center stripe width width value as a positive odd integer
correct = false;
int widthStripe = 0;
//run while loop until proper input is recieved
while (!correct){
  System.out.println("Please enter a positive odd integer for the width of the argyle center stripe: ");
  if (myScanner.hasNextInt()){
  widthStripe = myScanner.nextInt();
  if((widthStripe % 2 == 1) ){
    if( (widthStripe <= (widthDiamond / 2)) ){
      correct = true;
    }
  }
  else{
    widthDiamond = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}
      
//obtain first character for pattern fill
System.out.println("Please enter a first character for the pattern fill: ");
String temp = myScanner.next();
char cOne = temp.charAt(0);
      
//obtain second character for pattern fill
System.out.println("Please enter a second character for the pattern fill: ");
temp = myScanner.next();
char cTwo = temp.charAt(0);
      
//obtain third character for stripe fill
System.out.println("Please enter a third character for the stripe fill: ");
temp = myScanner.next();
char cThree = temp.charAt(0);

//initialize variables to be used in and outside the for loops
int j, m, l;
j = 0;
int fillDiamond = j * 2;
int fillBack = (widthDiamond*2) - fillDiamond;
int fillStart = fillBack/2;
int fillEnd = fillBack/2;

//Below this line outputs the argyle pattern from the above inputs

//For loop determines the height of the pattern
for (int i = 0; i < heightWindow; i++){
	// determines where to start repeat pattern
	j = i % (widthDiamond*2);
	if (j > widthDiamond){
		j = (widthDiamond*2) - j;
	}
	//for loop determines the width of the pattern
	for (int k = 0; k < widthWindow; k++){
		// determines where to start repeat pattern in width 
		m = k % (widthDiamond*2);
		if (m == 0) {
			fillDiamond = j *2;
			fillBack = (widthDiamond * 2) - fillDiamond;
			fillStart = fillBack/2;
			fillEnd = fillBack/2;
			
		}
		
		//
		if (i%(widthDiamond*2)>widthDiamond){
			l = (widthDiamond*2) - (i%(widthDiamond*2)) - 1;
		}
		else{
			l = j;
		}
		// determine positioning for the width of the stripe
		boolean forwardStripe, backwardStripe;
		backwardStripe = (m >= l - widthStripe/2) && (m <= l + widthStripe/2);
		forwardStripe = (m >= ((widthDiamond*2)-l-1-widthStripe/2)) && (m <= ((widthDiamond*2)-l-1+widthStripe/2));
		
		//calculations for the start
		if (fillStart > 0){
			//prints stripe to start
			if(backwardStripe || forwardStripe){
				System.out.print(cThree);
			}
			//if it doesnt start with a stripe print the background fill
			else{
				System.out.print(cOne);
			}
			fillStart--;
		}
		//Calculations for the diamond
		else if (fillDiamond > 0) {
			//print the stripe fill
			if( forwardStripe || backwardStripe){
				System.out.print(cThree);
			}
			//print the second fill
			else{
				System.out.print(cTwo);
			}
			fillDiamond--;
		}
		//after diamond
		else if (fillEnd > 0){
			//print the stripe fill
			if (backwardStripe || forwardStripe){
				System.out.print(cThree);
			}
			//print the first fill
			else {
				System.out.print(cOne);
			}
			fillEnd--;
		}
	}
	//creates a new line
	System.out.println();
}

	}  //end of main method   
} //end of class