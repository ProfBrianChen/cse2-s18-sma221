// Shane Acoveno
// 2/16/18
// CSE2-210
// lab04

// Card Generator
// Purpose:
// Generate a random card from a deck of 52
public class CardGenerator{
    	// main method
   	public static void main(String[] args) {
      
      //generate a random number from 0 to 52
      int randomNum = (int) (Math.random() * 52)+1;
      
      //declare variables for random card
      String cardSuit = " ";
      String cardIdentity = " ";
      
      //use if else statements to assign card suit
      if (randomNum <= 13){
        cardSuit = "diamonds";
      }
      else if (randomNum <= 26){
        cardSuit = "clubs";
      }
      else if (randomNum <= 39){
        cardSuit = "hearts";
      }
      else if (randomNum <= 52){
        cardSuit = "spades";
      }
      
      //use the modulus operator to determine the card identity
      int identityNum = randomNum % 13;
      
      //use switch statements to assign the card identity
      switch( identityNum ){
        case 0:
          cardIdentity = "king";
          break;
        case 1:
          cardIdentity = "ace";
          break;
        case 2:
          cardIdentity = "2";
          break;
        case 3:
          cardIdentity = "3";
          break;
        case 4:
          cardIdentity = "4";
          break;
        case 5:
          cardIdentity = "5";
          break;
        case 6:
          cardIdentity = "6";
          break;
        case 7:
          cardIdentity = "7";
          break;
        case 8:
          cardIdentity = "8";
          break;
        case 9:
          cardIdentity = "9";
          break;
        case 10:
          cardIdentity = "10";
          break;
        case 11:
          cardIdentity = "jack";
          break;
        case 12:
          cardIdentity = "queen";
          break;
      }
      
     //Print the resulting random card identity and suit
      System.out.println("You picked the "+cardIdentity +" of "+cardSuit);
        
      
      

}  //end of main method   
  	} //end of class

