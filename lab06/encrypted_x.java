// Shane Acoveno
// 3/9/18
// CSE2-210

//import Scanner class
import java.util.Scanner;

//encrypted_x
public class encrypted_x {
    	// main method
   	public static void main(String[] args) {
      
//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );
      
//check if input is an integer
boolean correct = false;
int input = 0;

//run while loop until proper input is recieved
while (!correct){
  System.out.println("Enter a positive integer between 0 and 100: ");
  if (myScanner.hasNextInt()){
  input = myScanner.nextInt();
  if(input > 0 && input < 100){
    correct = true;
  }
  else{
    input = 0;
  }
}
else {
  String junkWord = myScanner.next();
}
}
      
//outer loop determines how many lines will be printed
for(int i = 0; i < input; i++){
  
  for(int j = 0; j < input; j++){
   if (i == j || (input - i) - 1 == j){
    System.out.print(" ");
   }
   else{
     System.out.print("*");
   }
  
  
}
  System.out.println();
}
	}  //end of main method   
} //end of class

