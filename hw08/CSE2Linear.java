//Shane Acoveno
// 4/7/18
// CSE2 Hw08 
// CSE2Linear

//import scanner class
import java.util.Scanner;

//import random class
import java.util.Random;

public class CSE2Linear {
  
  //print array method
  public static void printArray( int [] array ){
    
    for( int j = 0; j < array.length; j++)
    System.out.print(array[j] + " ");
  }
  
  //binary search method
  public static void binarySearch( int [] array, int num){
    int middle =  ((int)(array.length / 2)) - 1;
    int iterations = 0;
    int previous = 0;
    int n = 0;
    
    for(iterations = 1; iterations < 8; iterations ++){
      
      if(array[middle] == num ){
        System.out.println(num + " was found in the list with " + iterations + " iterations");
        return;
      }
      else if (num > array[middle]){
        previous = middle;
        middle =  (middle +   ((array.length - middle) / 2)) ;
      }
      else if (num < array[middle]){
        n++;
        middle =  (middle -   ((middle - previous)/ 2))  ;
        if(n > 6){
          System.out.println(num + " was found in the list with " + iterations + " iterations");
          return;
        }
      }
    }
    System.out.println(num + " was not found in the list with " + iterations + " iterations");
    
  }
  
  //random scramble method
  public static int [] randomScramble( int [] array){
    
    for (int k = 0; k < array.length; k ++){
      //find random member to swap with
      int target = (int) (array.length * Math.random() );
      
      //swap the values
      int temp = array[target];
      array[target] = array[k];
      array[k] = temp;
    }
    
    //
    return array;
    
  }
  
  //linear search method
  public static void linearSearch( int [] array, int num){
   int l = 1;
   for(l = 1; l < array.length + 1; l++){
      
      if(array[l - 1]==num){
        System.out.println(num + " was found in the list with " + l + " iterations");
        return;
      }
     }
      System.out.println(num + " was not found in the list with "+ l + " iterations");
  }
  
  
  
  //main method
  public static void main(String[] args){
    
    //create instance of scanner 
    Scanner myScanner = new Scanner( System.in );
    
    //declarate and allocate finalGrades array
    final int num_grades = 15;
    int [] finalGrades = new int [num_grades];
    
    //prompt user for 15 ascending integers between 0 and 100
    System.out.println("Enter 15 ascending integers (between 0 & 100) for final grades in CSE2: ");
    
    //input validation and array initialization
    String junk = "";
    int previousInt = -1;
    int currentInt = 1;
    int i = 0;
    while(i < num_grades){
      if(!myScanner.hasNextInt()){
        junk = myScanner.next();
        System.out.println("ERROR: Please enter an integer type: ");
      }
      else if(myScanner.hasNextInt()){
        currentInt = myScanner.nextInt();
        if(currentInt < 0 || currentInt > 100){
          System.out.println("ERROR: Please enter an integer type between 0 & 100: ");
        }
        else if(currentInt < previousInt){
          System.out.println("ERROR: Please enter grades in ascending order");
        }
        else{
          finalGrades[i] = currentInt;
          previousInt = currentInt;
          i++;
        }
      }
     }
    
    //call print method for original array
    printArray(finalGrades);
    
    //prompt user for a grade to be searched for
    System.out.println("Enter a grade to search for: ");
    int numSearch = myScanner.nextInt();
    
    //call the binary search method
    binarySearch(finalGrades, numSearch);
    
    //call the random scrambling method and print method
    randomScramble(finalGrades);
   System.out.print("Scrambled: "); printArray(finalGrades);
    
    //prompt user for integer to be searched for
    System.out.println("Enter a grade to search for: ");
    numSearch = myScanner.nextInt();
    
    //call the linear search method
    linearSearch(finalGrades, numSearch);
    
  }  
}