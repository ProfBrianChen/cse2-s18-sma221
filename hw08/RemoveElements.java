import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      //check to make sure index is not out of bounds
      do{
      System.out.print("Enter the index ");
      index = scan.nextInt();
      }while(index < 0 || index > 10);
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
       
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  //random input method
  public static int [] randomInput(){
    int num[]=new int[10];
    int val = 0;
    
    for(int j = 0; j < 10; j++){
      val = (int) (Math.random() * 10);
      num[j] = val;
    }
    return num;
    
  }
  
  //delete method
  public static int [] delete(int [] list, int pos){
    int[] smallerArray = new int[9];
    
    for(int m = 0; m < pos; m++){
     smallerArray[m] = list[m];
    }
    
    for(int n = pos; n < 9; n++){
      smallerArray[n] = list[n + 1];
    }
    
    return smallerArray;
    
  }
  
  //remove method
  public static int [] remove(int [] list, int target){
 
 int k = 0;
 int [] storeIndex = new int[10]; //create array to store up to 10 indexes
    //implement linear search and find the indexes of the nums that match the target
    for(int p = 0; p < 10; p++){
      if(target == list[p]){
        storeIndex[k] = p; //store the index where the num matches the target
        k++;
      }
    }
    
 //create new array and determine length based upon how many target matches 
   int [] targetArray = new int[10 - k];
   
   for(int q = 0; q < k; q++){
   for(int m = 0; m < storeIndex[q]; m++){
     targetArray[m] = list[m];
    }
    
    for(int n = storeIndex[q] ; n < 10-k; n++){
      targetArray[n] = list[n+1];
    }
   }
    return targetArray;
  }
  
  
}
