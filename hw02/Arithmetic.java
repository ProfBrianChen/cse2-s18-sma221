// Shane Acoveno
// 2/2/18
// CSE2-210
// hw02

// Arithmetic
public class Arithmetic {
    	// main method
   	public static void main(String[] args) {
// input variables

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
      
// intermediate variables
double totalCostOfPants; //total cost of pants
double totalCostOfShirts; //total cost of sweatshirts
double totalCostOfBelts; //total cost of belts
double salesTaxPants; //sales tax charged on pants
double salesTaxShirts; //sales tax charged on sweatshirts
double salesTaxBelts; //sales tax charged on belts
double totalCostNoTax; //total cost of purchases before tax
double totalSalesTax; //total cost of sales tax
double totalCostWithTax; //total paid for this transaction, including sales tax

// run the calculations for total cost of each item
totalCostOfPants = pantsPrice * numPants;
totalCostOfShirts = shirtPrice * numShirts;
totalCostOfBelts = beltCost * numBelts;
// display total costs of each item
System.out.println("The total cost of pants is $"+totalCostOfPants);
System.out.println("The total cost of shirts is $"+totalCostOfShirts);
System.out.println("The total cost of belts is $"+totalCostOfBelts);

// run the calculations for sales tax of each item
salesTaxPants = totalCostOfPants * paSalesTax;
salesTaxShirts = totalCostOfShirts * paSalesTax;
salesTaxBelts = totalCostOfBelts * paSalesTax;
// eliminate fractional pennies and display total costs of sales tax of each item
int salesTaxPantsInt = (int) (salesTaxPants * 100); 
System.out.println("The sales tax of pants is $"+(salesTaxPantsInt/100.0));

int salesTaxShirtsInt = (int) (salesTaxShirts * 100);
System.out.println("The sales tax of shirts is $"+(salesTaxShirtsInt/100.0));

int salesTaxBeltsInt = (int) (salesTaxBelts * 100);
System.out.println("The sales tax of belts is $"+(salesTaxBeltsInt/100.0));

// run the calculations for total cost of purchases before tax
totalCostNoTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;

// display the total cost before tax
System.out.println("The total cost of purchases before tax is $"+totalCostNoTax);

// run the calculations for the total cost of sales tax
totalSalesTax = salesTaxPants + salesTaxBelts + salesTaxShirts;

// display the total cost of sales tax and eliminate fractional pennies
int totalSalesTaxInt = (int) (totalSalesTax * 100);
System.out.println("The total sales tax is $"+(totalSalesTaxInt/100.0));

// run the calculations for the total cost with tax and eliminate fractional pennies
totalCostWithTax = totalCostNoTax + totalSalesTax;
int totalCostWithTaxInt = (int) (totalCostWithTax * 100);
System.out.println("The total cost of purchases with sales tax is $"+(totalCostWithTaxInt/100.0));

	}  //end of main method   
} //end of class
