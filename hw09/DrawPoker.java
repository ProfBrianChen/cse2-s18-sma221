//Shane Acoveno
//CSE 2
//hw09 DrawPoker
// 4/16/18


import java.util.Random;
public class DrawPoker {
  
  //shuffle method
  public static int [] shuffle( int [] array){
    
    for (int k = 0; k < array.length; k ++){
      //find random member to swap with
      int target = (int) (array.length * Math.random() );
      
      //swap the values
      int temp = array[target];
      array[target] = array[k];
      array[k] = temp;
    }
    return array; 
  }
  
  //pair method
  public static boolean pair( int [] array){
    
    //check to see if hand contains pair utilizing modulur division
    for(int i = 0; i < 5; i++){
      for(int j = 1; j < 5; j++){
        int a = array[i];
        int b = array[j];
        if((a%13)==(b%13)){
          return true;
          
        }
      }
    }
    return false;
    
  }
  
  //three of a kind method
  public static boolean threeOfaKind( int [] array){
    for(int i = 0; i < 5; i++){
      int test = array[i];
      for(int j = 0; j < 5; j++){
        if(test%13 == array[j]%13){
          int test2 = array[j];
          for(int k = 0; k < 5; k++){
            if(test2%13 == array[k]%13 && array[k] != test && array[k] != test2 && test2 != test){
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  //flush method
  public static boolean flush(int [] array){
    int test = array[0];
    int count = 0;
    
    for( int i = 1; i < 5; i++){
      if(test/13 == array[i]/13){
        count = count +1;
      }
    }
    if (count == 4){
      return true;
    }
    return false;
  }
  
  //full house method
  public static boolean fullHouse(int[] array){
    int count = 0;
    
    for(int i = 0; i < 5; i++){
      int test = array[i];
      for(int j = 0; j < 5; j++){
        if(test%13 == array[j]%13 && test != array[j]){
          count++;
          
          break;
        }
      }
    }
    if (count == 5){
      return true;
    }
    return false;
  }
  
  
  //main method
  public static void main(String args[]){
  
    //declare and allocate decok of cards array
    int [] deckOfCards = new int [52];
    
    //array inilitialization
    for(int i = 0; i < deckOfCards.length; i++){
      deckOfCards [i] = i;
    }
    
    //call shuffle method
    int [] shuffledDeck = new int [52];
    shuffledDeck = shuffle(deckOfCards);
    
    //create hand arrays for both players 
    int [] playerOneHand = new int [5];
    int [] playerTwoHand = new int [5];
    
    //deal hands to each player
    int k = 0;
    int l = 0;
    for(int j = 0; j < 10; j++ ){
      if((j%2)==0){
        playerOneHand[k] = shuffledDeck [j];
        k++;
      }
      else{
        playerTwoHand[l] = shuffledDeck [j];
        l++;
      }
   }
    //Print player hands to keep track
    System.out.print("Player One Hand: ");
    for(int z = 0; z< 5; z++){
        System.out.print(" "+ playerOneHand[z]);
      }
    System.out.println("");
    System.out.print("Player Two Hand: ");
    for(int z = 0; z< 5; z++){
        System.out.print(" "+ playerTwoHand[z]);
      }
    
    //print methods
    System.out.println(" ");
    System.out.println("Pair 1: "+ pair(playerOneHand));
    System.out.println("Pair 2: " +pair(playerTwoHand));
    System.out.println("Three of a kind 1: "+ threeOfaKind(playerOneHand));
    System.out.println("Three of a kind 2: " +threeOfaKind(playerTwoHand));
    System.out.println("Flush 1: " +flush(playerOneHand));
    System.out.println("Flush 2: "+ flush(playerTwoHand));
    System.out.println("Full House 1: "+ fullHouse(playerOneHand));
    System.out.println("Full House 2: "+ fullHouse(playerTwoHand));
    
  }
 }