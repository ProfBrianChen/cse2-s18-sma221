// Shane Acoveno
// 2/2/18
// CSE 2 lab02
// program purpose:
// print the number of minutes for each trip
// print the number of counts for each trip
// print the distance of each trip in miles
// print the distance for the two trips combined

//
public class Cyclometer {
    	// main method
   	public static void main(String[] args) {
// input data
      int secsTrip1=480;  // time in seconds of trip 1
      int secsTrip2=3220;  // time in seconds of trip 2
      int countsTrip1=1561;  // number of front wheel rotations for trip 1
      int countsTrip2=9037; // number of front wheel rotations for trip 2
      
// intermediate variables and output data
    double wheelDiameter=27.0,  // diamter of the front wheel
  	PI=3.14159, // conmstant value of PI
  	feetPerMile=5280,  // number of feet in a mile
  	inchesPerFoot=12,   // number of inches in a foot
  	secondsPerMinute=60;  // number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // variables for trip 1, trip 2, and total trip distance

// print seconds and counts for both trips      
System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

//run the calculations and store the variables
//calcuate distance of trip 1, 2, and total 
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2; // Gives total distance 
      
//Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");



      
	}  //end of main method   
} //end of class