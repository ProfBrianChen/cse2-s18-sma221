// Shane Acoveno
// 3/4/18
// CSE2-210

//import Scanner class
import java.util.Scanner;

//Hw05
//Purpose: prompt user for certain types and use loops until proper types are inputted
public class Hw05 {
    	// main method
   	public static void main(String[] args) {

//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );
      
//create input verification boolean and initialize course number
boolean verification = false;
int courseNum = 0;
String junkWord = "";
//run while loop until proper integer is entered for the course number
System.out.println("Enter the course number: "); // prompt user for input
while (!verification){
  if (myScanner.hasNextInt()){
  courseNum = myScanner.nextInt();
  if(courseNum > 0){
    verification = true;
  }
  else{
    courseNum = 0;
    System.out.println("ERROR: Enter the course number as a positive integer type: "); //display error message, prompt user for proper type
  }
}
else {
  junkWord = myScanner.next();
  System.out.println("ERROR: Enter the course number as a positive integer type: "); //display error message, prompt user for proper type
}
}

 verification = false;
 String name = " ";
 double junkDouble = 0;
//run while loop until proper string is entered for course department name 
 System.out.println("Enter the course department name: "); // prompt user for input     
while (!verification){
  if (myScanner.hasNextDouble()){
    junkDouble = myScanner.nextDouble();
    System.out.println("ERROR: Enter the course department name as a string type: ");
  }
  else if(myScanner.hasNext()){
    name = myScanner.next();
    verification = true; // in order to exit the loop
  }
  }
      
 verification = false;
 int perWeek = 1;
 //run while loop until proper integer is entered for number of times it meets a week
 System.out.println("Enter the number of times the course meets a week: "); // prompt user for input
 while (!verification){
  if (myScanner.hasNextInt()){
  perWeek = myScanner.nextInt();
  if(perWeek > 0){
    verification = true;
  }
  else{
    System.out.println("ERROR: Enter the number of times the course meets a week as a positive integer type: "); //display error message, prompt user for proper type
  }
}
else {
  junkWord = myScanner.next();
  System.out.println("ERROR: Enter the number of times the course meets a week as a positive integer type: "); //display error message, prompt user for proper type
}
}

verification = false;
int time = 0;
int length = 0; 
//run while loop until the class time is entered as the proper type in the proper format
System.out.println("Enter the time that the class starts in the form xxxx "); // prompt user for input
 while (!verification){
  if (myScanner.hasNextInt()){
  time = myScanner.nextInt();
  length = String.valueOf(time).length(); //get the number of digits entered
  if(time > 0 && length == 4){
    verification = true;
  }
  else{
    System.out.println("ERROR: Enter the time that the class starts as a postive integer type. e.g: 1036 "); //display error message, prompt user for proper type
  }
}
else {
  junkWord = myScanner.next();
  System.out.println("ERROR: Enter the time that the class starts as a postive integer type. e.g: 1037 "); //display error message, prompt user for proper type
}
}     

verification = false;
String professor = "";
System.out.println("Enter the first and last name of the professor separated by a space: "); // prompt user for input
//run while loop until the first and last name of the professor (separated by a space) is entered
while (!verification){
	if (myScanner.hasNextDouble()){
    junkDouble = myScanner.nextDouble();
    System.out.println("ERROR: Enter the first and last name of the professor separated by a space as a string type: "); //display error message, prompt user for proper type
  }
  else if(myScanner.hasNext()){
    professor = myScanner.next();
    if(myScanner.hasNextDouble()){
			junkDouble = myScanner.nextDouble();
			System.out.println("ERROR: Enter the first and last name of the professor separated by a space as a string type: "); //display error message, prompt user for proper type
		}
		else if(myScanner.hasNext()){
			professor = myScanner.next();
			verification = true;
		}
		}
  }
	
	verification = false;
  int students = 0;
	//run while loop until proper integer is entered for the number of students
System.out.println("Enter the number of students in the class: "); // prompt user for input
while (!verification){
  if (myScanner.hasNextInt()){
  students = myScanner.nextInt();
  if(students > 0){
    verification = true;
  }
  else{
    students = 0;
    System.out.println("ERROR: Enter the number of students as a positive integer type: "); //display error message, prompt user for proper type
  }
}
else {
  junkWord = myScanner.next();
  System.out.println("ERROR: Enter the number of students as a positive integer type: "); //display error message, prompt user for proper type
}
}
	     
	}  //end of main method   
} //end of class
