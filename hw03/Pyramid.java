// Shane Acoveno
// 2/11/18
// CSE2-210
// hw03

//import Scanner class
import java.util.Scanner;

//Pyramid
//purpose: prompt user for square side and height of pyramid and calculate volume
public class Pyramid{
    	// main method
   	  public static void main(String[] args) {

//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );
        
//prompt the user for the length of the square side of a pyramid
System.out.print("The square side of the pyramid is (input length): ");

//accept user input as a double
double length = myScanner.nextDouble();
        
//prompt the user for the height of the pyramid
System.out.print("The height of the pyramid is (input height): ");
        
//accept user input as a double
double height = myScanner.nextDouble();
        
//calculate volume inside pyramid
double baseArea = length * length; //area of base is length times length
double volume = baseArea * (height/3); //calculate volume inside pyramid using formula    

//display the volume of the pyramid
System.out.println("The volume inside the pyramid is: "+volume);
        
}  //end of main method   
  	} //end of class