// Shane Acoveno
// 2/9/18
// CSE2-210
// hw03

//import Scanner class
import java.util.Scanner;

//Convert
//purpose: convert acres and inches of rainfall into cubic miles
public class Convert{
	// main method
    public static void main(String[] args) {
  
//create instance that will take input from STDIN
Scanner myScanner = new Scanner( System.in );

//prompt the user for the affected area in acres 
System.out.print("Enter the number of acres of land affected by hurricane precipitation: ");

//accept user input as a double
double acres = myScanner.nextDouble();
      
//prompt the user for how many inches of rain were dropped on average
System.out.print("Enter the average amount of rain dropped in inches: ");
      
//accept user input as a double
double inches = myScanner.nextDouble();

//convert acres to square miles 
double squareMilesInAcre = 0.0015625; //number of square miles in one acre
double squareMiles = acres * squareMilesInAcre; //square miles
      
//convert inches of rain to miles of rain
int inchesInAFoot = 12; //number of inches in a foot
int feetInAMile = 5280; //number of feet in a mile 
double feet = inches/inchesInAFoot;
double miles = feet/feetInAMile;
      
//multiply square miles by miles to get cubic miles
double cubicMiles = squareMiles * miles;
      
//display cubic miles
System.out.println(cubicMiles+" cubic miles");

}  //end of main method   
  	} //end of class