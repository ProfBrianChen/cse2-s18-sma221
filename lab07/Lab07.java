// Shane Acoveno
// 3/25/18
// CSE2 lab 07

//import Scanner class
import java.util.Scanner;

//Lab 07
public class Lab07 {
  
  //Create random adjective method
  //Create the random object 
  
  public static String Adjective(){
    String adj = "";
    int randomInt = (int) (Math.random() * 9)+1;
    
    //choose adjective using switch statement
    switch(randomInt){
      case 1:
        adj = "big";
        break;
      case 2:
        adj = "brave";
        break;
      case 3:
        adj = "loud";
        break;
      case 4:
        adj = "angry";
        break;
      case 5:
        adj = "happy";
        break;
      case 6:
        adj = "scary";
        break;
      case 7:
        adj = "little";
        break;
      case 8:
        adj = "smart";
        break;
      case 9:
        adj = "old";
        break;
    }
    return adj; 
    }
  
  public static String Subject(){
    //Create the random object 
  
    String sub = "";
    int randomInt = (int) (Math.random() * 9)+1;
    
    //choose subject using switch statement
    switch(randomInt){
      case 1:
        sub = "dog";
        break;
      case 2:
        sub = "cat";
        break;
      case 3:
        sub = "car";
        break;
      case 4:
        sub = "truck";
        break;
      case 5:
        sub = "shovel";
        break;
      case 6:
        sub = "boat";
        break;
      case 7:
        sub = "student";
        break;
      case 8:
        sub = "oven";
        break;
      case 9:
        sub = "turtle";
        break;
    }
    return sub; 
    }
  
  public static String Verb(){
    //Create the random object 
  
    String ver = "";
    int randomInt = (int) (Math.random() * 9)+1;
    
    //choose verb using switch statement
    switch(randomInt){
      case 1:
        ver = "drank";
        break;
      case 2:
        ver = "ate";
        break;
      case 3:
        ver = "ran";
        break;
      case 4:
        ver = "fell";
        break;
      case 5:
        ver = "joined";
        break;
      case 6:
        ver = "called";
        break;
      case 7:
        ver = "calculated";
        break;
      case 8:
        ver = "jumped";
        break;
      case 9:
        ver = "swam";
        break;
    }
    return ver; 
    }
  
  public static String Object1(){
    //Create the random object 
  
    String obj = "";
    int randomInt = (int) (Math.random() * 9)+1;
    
    //choose object using switch statement
    switch(randomInt){
      case 1:
        obj = "squirrel";
        break;
      case 2:
        obj = "shark";
        break;
      case 3:
        obj = "motorcycle";
        break;
      case 4:
        obj = "bird";
        break;
      case 5:
        obj = "fork";
        break;
      case 6:
        obj = "battery";
        break;
      case 7:
        obj = "student";
        break;
      case 8:
        obj = "computer";
        break;
      case 9:
        obj = "professor";
        break;
    }
    return obj; 
    }
  
  //thesis sentence method
  public static String Thesis(){
    //create instance that will take input from STDIN
  Scanner myScanner = new Scanner( System.in ); 
    
  String adjective1 = "";
  String adjective2 = "";
  String subject = "";
  String verb = "";
  String adjective3 = "";
  String object = "";
    for(int i = 0; i < 6; i++){
      switch(i){
        case 0:
          adjective1 = Adjective();
          System.out.print("The "+adjective1);
        break;
        case 1:
          adjective2 = Adjective();
          System.out.print(" " + adjective2);
          break;
        case 2:
          subject = Subject();
          System.out.print(" "+ subject);
          break;
        case 3:
          verb = Verb();
          System.out.print(" "+verb);
          break;
        case 4:
          adjective3 = Adjective();
          System.out.print(" the "+adjective3);
          break;
        case 5:
          object = Object1();
          System.out.print(" "+object+".");
          System.out.println("");
          System.out.print("Would you like another sentence? If so enter '1'. Otherwise, enter '2'. ");
          //accept user input as an Int
          int userChoice = myScanner.nextInt();
          if (userChoice == 1){
            i = -1;
          }
          break;
         }
     
      
    }
    return subject;
  }
  
  public static String Action(String subject){
    
    
    int randomAction = (int) (Math.random() * 4)+1;
    //randomly pick using the subject or it
    for(int j = 0; j < randomAction; j++){// for loop in order to run action sentences a random number of times
      int randomInt = (int) (Math.random() * 2)+1;
    if(randomInt == 1 ){
      for(int i = 0; i < 5; i++){
      switch(i){
        case 0:
          
          System.out.print("This "+subject);
        break;
        case 1:
          System.out.print(" was "+Adjective());
          break;
        case 2:
          System.out.print(" " +Verb());
          break;
        case 3:
          System.out.print(" to "+ Adjective());
          break;
        case 4:
          System.out.print(" " +Object1()+".");
          System.out.println(" ");
          break;
        }}}
     if(randomInt == 2){
       for(int i = 0; i < 6; i++){
         switch(i){
           case 0:
             System.out.print("It used ");
             break;
           case 1:
              
             System.out.print(Subject()+ " to ");
             break;
           case 2:
             System.out.print(Verb());
             break;
           case 3:
             System.out.print(" "+Object1()+" at the " );
             break;
           case 4:
             System.out.print(Adjective() + " ");
             break;
           case 5:
             System.out.print(Object1()+".");
             System.out.println("");
             break;
         }
       }
     }
    }
    return subject;
   }
   
   public static void Conclusion(String subject){
     for(int i = 0; i < 3; i++){
       switch(i){
         case 0:
           System.out.print("That "+subject);
            break;
         case 1:
           System.out.print(" "+ Verb());
           break;
         case 2:
           System.out.print(" the " + Object1()+"!");
           System.out.println("");
           break;  
         
       }
     }
   }
  public static void Final(){
    Conclusion(Action(Thesis()));
  }
  
  // main method
  
  public static void main(String[] args){
    Final();
    
    }
  }
